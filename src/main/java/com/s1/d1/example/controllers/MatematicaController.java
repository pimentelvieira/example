package com.s1.d1.example.controllers;

import com.s1.d1.example.models.MatematicaModel;
import com.s1.d1.example.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("/soma")
    public Integer soma(@RequestBody MatematicaModel matematicaModel) {
        return matematicaService.soma(matematicaModel.getNumeros());
    }

    @PostMapping("/subtracao")
    public Integer subtracao(@RequestBody MatematicaModel matematicaModel) {
        return matematicaService.subtracao(matematicaModel.getNumeros());
    }

    @PostMapping("/multiplicacao")
    public Integer multiplicacao(@RequestBody MatematicaModel matematicaModel) {
        return matematicaService.multiplicacao(matematicaModel.getNumeros());
    }

    @PostMapping("/divisao")
    public Integer divisao(@RequestBody MatematicaModel matematicaModel) {
        return matematicaService.divisao(matematicaModel.getNumeros());
    }
}
