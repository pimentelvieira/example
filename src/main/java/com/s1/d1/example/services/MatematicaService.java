package com.s1.d1.example.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class MatematicaService {

    public Integer soma(List<Integer> numeros) {
        if (numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ao menos dois números para serem somados");
        }

        int resultado = 0;

        for (Integer numero : numeros) {
            resultado += numero;
        }

        return resultado;
    }

    public Integer subtracao(List<Integer> numeros) {
        if (numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ao menos dois números para serem subtraidos");
        }

        int resultado = numeros.get(0);

        for (int i = 1; i < numeros.size(); i++) {
            resultado -= numeros.get(i);
        }

        return resultado;
    }

    public Integer multiplicacao(List<Integer> numeros) {
        if (numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ao menos dois números para serem multiplicados");
        }

        int resultado = 1;

        for (Integer numero : numeros) {
            resultado *= numero;
        }

        return resultado;
    }

    public Integer divisao(List<Integer> numeros) {
        if (numeros.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Para divisão enviar dois números");
        }

        int resultado = numeros.get(0);

        for (int i = 1; i < numeros.size(); i++) {
            if (resultado < numeros.get(i)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Dividendo deve ser maior ou igual ao divisor");
            } else {
                resultado /= numeros.get(i);
            }
        }

        return resultado;
    }
}
